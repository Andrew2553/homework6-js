// 1. Экранирование - это процесс добавления специальных символов или последовательностей символов в строки или тексты в языках программирования, чтобы изменить их интерпретацию. Это позволяет вставлять в строки символы, которые в противном случае имели бы специальное значение или влияли бы на саму структуру строки.

// 2. Объявление функции с помощью ключевого слова function;
//    Выражение функции: let myFunction = function() {};
//    Стрелочная функция;
//    Объявление функции через метод объекта;
//    Функции-конструкторы;

// 3. Hoisting - это механизм, который поднимает объявления переменных и функций вверх в области видимости перед выполнением кода. Это означает, что вы можете использовать переменные и функции, даже если их объявление появляется позже в коде.

 function createNewUser() {
    const newUser = {
      firstName: prompt("Введіть ваше ім'я:"),
      lastName: prompt("Введіть ваше прізвище:"),
      birthday: prompt("Введіть дату свого народження (dd.mm.yyyy):")
    };
  
    newUser.getLogin = function() {
      const login = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
      return login;
    };
  
    newUser.getAge = function() {
        const today = new Date();
        const birthDateParts = this.birthday.split('.');
        const birthDay = parseInt(birthDateParts[0], 10);
        const birthMonth = parseInt(birthDateParts[1], 10) - 1; 
        const birthYear = parseInt(birthDateParts[2], 10);
        const birthDate = new Date(birthYear, birthMonth, birthDay);
      
        const age = today.getFullYear() - birthDate.getFullYear();
        const isBirthdayPassed = today.getMonth() > birthDate.getMonth() ||
                                 (today.getMonth() === birthDate.getMonth() && today.getDate() >= birthDate.getDate());
        if (!isBirthdayPassed) {
          return age - 1;
        }
      
        return age;
      };
  
    newUser.getPassword = function() {
      const firstLetter = this.firstName.charAt(0).toUpperCase();
      const lastName = this.lastName.toLowerCase();
      const yearOfBirth = this.birthday.substring(6, 10);
      const password = firstLetter + lastName + yearOfBirth;
      return password;
    };
  
    return newUser;
  }
  
  const user = createNewUser();
  console.log(user.getLogin());
  console.log(user.getAge());
  console.log(user.getPassword());